<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;



class CategoryController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/v1/categories",
     *   tags={"v1/categories"},
     *   summary="Summary Categories",
     *   operationId="GetAllCategories",
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=406, description="not acceptable"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function GetAllCategories()
    {
        $categories = Category::get();
        $count = 0;
        $status = false;
        $message = '';

        if ($categories == null) {
            $count = 0;
            $status = false;
            $message = 'No Categories Found!!!';
        } else {
            $count = count($categories);
            $status = true;
            $message = 'Categories Found!!!';
        }
        return ['data' => $categories, 'status' => $status, 'count' => $count, 'messages' => $message];
    }

    /**
     * @SWG\Delete(
     *     path="/v1/categories/{CategoryId}",
     *     tags={"v1/categories"},
     *     summary="Deletes a category",
     *     description="អត់មានអីទេ",
     *     operationId="GetCategoriesById",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Category id to delete",
     *         in="path",
     *         name="CategoryId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid ID supplied"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Category not found"
     *     )
     * )
     */
    public function GetCategoriesById($CategoryId)
    {
        $category = Category::find($CategoryId);
        $category->delete();
        return ['status' => true, 'messages' => "Category delete successfully!!!"];
    }

//    public function GetCategoriesById(Request $request) {
//        $inputs = $request->input();
//
//        $category_id = $inputs['category-id'];
//        $category_name = $inputs['category-name'];
//        $category_slug = str_slug($category_name);
//
//        $input_to_validate = $request->all();
//        $rules = ['category-name' => 'required|string'];
//
//        $validator = Validator::make($input_to_validate, $rules);
//
//        if ($validator->fails()) {
//            var_dump($validator);
//            return;
//        }
//
//        $category = Category::find($category_id);
//
//        $category->name = $category_name;
//        $category->slug = $category_slug;
//
//        /*date_default_timezone_set("Asia/Phnom_Penh");
//        $category->updated_at = date("Y-m-d h:i:s");
//        $category->created_at = date("Y-m-d h:i:s");*/
//
//        $category->save();
//
//        return ['status' => true, 'messages' => "Update category success!!!"];
//    }
}
