<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @SWG\Swagger(
 *   basePath="/S2-Linux-Assignment/public/api",
 *   @SWG\Info(
 *     title="S2 Linux Assignment API",
 *     version="1.0.0"
 *   ),
 *    @SWG\Parameter(
 *      name="api_key",
 *      in="header",
  *     required=true,
 *      type="string"
 *   ),
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
